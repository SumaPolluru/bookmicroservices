package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.model.Book;
import com.example.demo.model.BookRepository;
import com.example.demo.model.RemoteBookRepository;

import java.util.*;

import javax.servlet.http.HttpServletRequest;
@RestController
public class BookConsumerController {
	@Autowired
	@Qualifier("b1")
	BookRepository remotebookrep;
	
	@RequestMapping("/")
	public ModelAndView Welcome(HttpServletRequest request) 
	{
		request.setAttribute("bookobj", "BOOK_DATA");
		return new ModelAndView("Form");
	}
	@GetMapping("/BookByName")
	public List<Book> getByName(@RequestParam String bookname)
	{
		List<Book> bookdet=remotebookrep.findByBookname(bookname);
		return bookdet;
				
	}
	@GetMapping("/data")
	public List<Book> getDet()
	{
		return remotebookrep.getDetails();
	}

}

package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import com.example.demo.model.BookRepository;
import com.example.demo.model.RemoteBookRepository;

@SpringBootApplication
@EnableDiscoveryClient
public class BookConsumerApplication {
	public static final String
	Book_SERVICE_URL = "http://Book-Microservice-Producer";


	public static void main(String[] args) {
		SpringApplication.run(BookConsumerApplication.class, args);
	}
	@Bean
	@LoadBalanced
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}
	@Bean(name="b1")	
	public BookRepository bookRepository(){
		return new RemoteBookRepository(Book_SERVICE_URL);
	}

}

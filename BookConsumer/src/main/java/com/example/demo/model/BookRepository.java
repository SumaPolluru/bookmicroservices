package com.example.demo.model;

import java.util.List;

import org.springframework.stereotype.Repository;

public interface BookRepository {
	public List<Book> findByBookname(String bookname);
	public List<Book> findByAuthorname(String authorname);
	public List<Book> getDetails();

}

package com.example.demo.model;
import java.util.*;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
@Component
public class RemoteBookRepository implements BookRepository {

	@Autowired
	protected RestTemplate restTemplate;
//	@Autowired
	//private DiscoveryClient discoveryClient;
	protected String serviceUrl;
	public RemoteBookRepository()
	{
		
	}
	//http://prof
	//profile-producer-microservice
	public RemoteBookRepository(String serviceUrl) {
		this.serviceUrl = serviceUrl.startsWith("http") ? serviceUrl
				: "http://" + serviceUrl;
	}
	
	//http://profile-microservices-producer/profiles
	@Override
	public List<Book> findByBookname(String bookname) {
		System.out.println("Service url is "+serviceUrl);
		Book[] bookdetails = restTemplate.getForObject
				(serviceUrl+"/Book/{bookname}", Book[].class,bookname);
	//	Profile[] profiles=restTemplate.getForObject(baseUrl1, Profile[].class);	
		return Arrays.asList(bookdetails);
	}
	@Override
	public List<Book> findByAuthorname(String authorname) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public List<Book> getDetails() {
		// TODO Auto-generated method stub
		System.out.println("Service url is "+serviceUrl);
		Book[] bookdetails = restTemplate.getForObject
				(serviceUrl+"/data", Book[].class);
	//	Profile[] profiles=restTemplate.getForObject(baseUrl1, Profile[].class);	
		return Arrays.asList(bookdetails);
		
	}
}

<!DOCTYPE html >
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Expires" content="sat, 01 Dec 2001 00:00:00 GMT">
<title>Admin|Home</title>
<link href="static/css/bootstrap.min.css" rel="stylesheet">
<link href="static/css/style.css" rel="stylesheet">


</head>
<body>
	<c:choose>
		<c:when test="${bookobj=='BOOK_DATA' }">
			<div class="container text-center">
				<h3>New Registration</h3>
				<hr>
				<form class="form-horizontal" action="/BookByName">
					<div class="form-group">
						<label class="control-label col-md-3">BookName</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="bookname"
								value="${book.bookname}" placeholder="bookname" required />
						</div>
					</div>
					<div class="form-group ">
						<input type="submit" class="btn btn-primary" value="Check" />
					</div>
				</form>
			</div>
		</c:when>
</c:choose>
	<script src="static/js/jquery-1.11.1.min.js"></script>
	<script src="static/js/bootstrap.min.js"></script>
</body>
</html>
package com.app.BookConsumerMicroservice.model;

import java.util.List;

import org.springframework.stereotype.Repository;

@Repository
public interface BookRepository {
	
	public List<Book> findByBookName(String bookName);
	
	public List<Book> findByAuthorName(String authorName);
	
	public List<Book> findByBookId(int bookId);

	public List<Book> getDetails();


	

	
	
	

}

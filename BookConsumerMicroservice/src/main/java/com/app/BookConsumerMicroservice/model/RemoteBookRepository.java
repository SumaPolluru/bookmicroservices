package com.app.BookConsumerMicroservice.model;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

@Component
@Repository
public class RemoteBookRepository implements BookRepository {

	// RestTemplate is defined as part of web services which can be used
	// to read data from any webserivce.
	@Autowired
	protected RestTemplate restTemplate;
//		@Autowired
	// private DiscoveryClient discoveryClient;
	protected String serviceUrl;

	public RemoteBookRepository() {

	}

	// http://prof
	// profile-producer-microservice
	public RemoteBookRepository(String serviceUrl) {
		this.serviceUrl = serviceUrl.startsWith("http") ? serviceUrl : "http://" + serviceUrl;
	}

	// http://profile-microservices-producer/profiles
	@Override
		public List<Book> findByBookName(String bookName) {
			System.out.println("Service url is "+serviceUrl);
			Book[] bookdetails = restTemplate.getForObject
					(serviceUrl+"/BookByBookName/{bookName}", Book[].class,bookName);
		//	Profile[] profiles=restTemplate.getForObject(baseUrl1, Profile[].class);	
			return Arrays.asList(bookdetails);
		}
	@Override
	public List<Book> findByAuthorName(String authorname) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public List<Book> getDetails() {
		// TODO Auto-generated method stub
		System.out.println("Service url is "+serviceUrl);
		Book[] bookdetails = restTemplate.getForObject
				(serviceUrl+"/data", Book[].class);
	//	Profile[] profiles=restTemplate.getForObject(baseUrl1, Profile[].class);	
		return Arrays.asList(bookdetails);
		
	}

	@Override
	public List<Book> findByBookId(int bookId) {
		// TODO Auto-generated method stub
		return null;
	}

}

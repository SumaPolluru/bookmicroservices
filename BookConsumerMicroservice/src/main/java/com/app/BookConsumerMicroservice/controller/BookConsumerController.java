package com.app.BookConsumerMicroservice.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.app.BookConsumerMicroservice.model.Book;
import com.app.BookConsumerMicroservice.model.BookRepository;

import java.util.*;
@RestController
public class BookConsumerController {
	@Autowired
	@Qualifier("b1")
	BookRepository remotebookrep;	
	@GetMapping("/BookByBookName/{bookName}")
	public List<Book> getByName(@PathVariable("bookName")String bookName)
	{
		List<Book> bookdet=remotebookrep.findByBookName(bookName);
		return bookdet;
				
	}
	@GetMapping("/data")
	public List<Book> getDet()
	{
		return remotebookrep.getDetails();
	}

}

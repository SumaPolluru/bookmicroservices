package com.app.BookConsumerMicroservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;
import com.app.BookConsumerMicroservice.model.RemoteBookRepository;
import com.app.BookConsumerMicroservice.model.BookRepository;

@SpringBootApplication
@EnableEurekaClient
public class BookConsumerMicroserviceApplication {
	public static final String Book_SERVICE_URL = "http://Book-Microservice-Producer";

	public static void main(String[] args) {
		SpringApplication.run(BookConsumerMicroserviceApplication.class, args);
	}

	@Bean
	@LoadBalanced
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

	@Bean(name ="b1")
	public BookRepository bookRepository() {
		return new RemoteBookRepository(Book_SERVICE_URL);
	}

}

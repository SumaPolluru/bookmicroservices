package com.app.bookProducer.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.bookProducer.model.Book;
import com.app.bookProducer.model.BookRepository;

@RestController
public class BookController {
	@Autowired
	BookRepository bookrep;

	@GetMapping("/data")
	public List<Book> getDetails() {
		return bookrep.findAll();
	}

	@RequestMapping("/insert")
	public Book insertData(Book b) {
		bookrep.save(b);
		return b;

	}

	@RequestMapping("/BookDetails/{id}")
	public List<Book> getDet(@PathVariable("id") int bookId) {
		List<Book> b = bookrep.findByBookId(bookId);
		return b;

	}

	@RequestMapping("/BookByBookName/{bookName}")
	public List<Book> getBookByName(@PathVariable("bookName") String bookName) {
		List<Book> booklist = bookrep.findByBookName(bookName);
		return booklist;
	}

	@RequestMapping("/BookDet/{authorname}")
	public List<Book> getBookByAuthorname(@PathVariable("authorname") String AuthorName) {
		List<Book> booklist = bookrep.findByAuthorName(AuthorName);
		return booklist;
	}

}

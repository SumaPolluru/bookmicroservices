package com.app.bookProducer.model;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface BookRepository extends JpaRepository<Book,Integer> {
	
	public List<Book> findByBookName(String bookName);
	
	public List<Book> findByAuthorName(String authorName);

	public List<Book> findByBookId(int bookId);
	
	
	
	

}

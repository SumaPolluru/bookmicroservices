
package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;
import com.example.demo.model.Book;
import com.example.demo.model.BookRepository;

@RestController
public class BookController {
	@Autowired
	BookRepository bookrep;
	@RequestMapping("/data")
	public List<Book> getDetails()
	{
		return bookrep.findAll();
	}
	@RequestMapping("/insert")
	public Book insertData(Book b)
	{
		bookrep.save(b);
		return b;
	}
	@RequestMapping("/BookDetails/{id}")
	public Book getDet(@PathVariable("id") int bookid)
	{
		Book b=bookrep.getById(bookid);
		return b;
		
	}
	@RequestMapping("/Book/{bookname}")
	public List<Book> getBookByName(@PathVariable("bookname")String bookname)
	{
		List<Book> booklist=bookrep.findByBookname(bookname);
		return booklist;
	}
	@RequestMapping("/BookDet/{authorname}")
	public List<Book> getBookByAuthorname(@PathVariable("authorname")String Authorname)
	{
		List<Book> booklist=bookrep.findByBookname(Authorname);
		return booklist;
	}
	
	
	

}

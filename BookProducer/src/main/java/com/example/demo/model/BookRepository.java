package com.example.demo.model;
import java.util.*;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookRepository extends JpaRepository<Book,Integer>{
	
	public List<Book> findByBookname(String bookname);
	public List<Book> findByAuthorname(String authorname);

}
